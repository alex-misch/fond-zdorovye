<?php

/**
 * Class Stat_model
 *
 * @property CI_DB_query_builder|CI_DB_result $db
 */
class Stat_model extends CI_Model {

  var $table = 'stat2018';


  function getList($region_id = 0, $arParams = []) {

    $this->db->select(array(
//      'Value2017 as rating_2017',
//      'Value2018 as rating_2018',

      'AVG(ValueDynamic) as dynamic',

      'SUM(Value2017) as rating_2017',
      'SUM(Value2018) as rating_2018',

      'month'

//      'MAX(Value2018) as max_2018',
//      'MIN(Value2018) as min_2018',
//
//      'MAX(Value2017) as max_2017',
//      'MIN(Value2017) as min_2017',
//
//      'case when MAX(Value2018) > MAX(Value2017) then MAX(Value2018) else MAX(Value2017) end as max_value',
//      'case when MIN(Value2018) < MIN(Value2017) then MIN(Value2018) else MIN(Value2017) end as min_value',


    ));
    if ($region_id)
      $this->db->where('s.idR', $region_id);

    if ( $arParams && count($arParams) > 0 ) {
      $this->db->join("param2018 p", "p.id = s.idP", 'inner');
      $this->db->where_in('p.id', $arParams);
    }
    $this->db->group_by('s.month');

    $this->db->from("$this->table s");
    return $this->db->get()->result_array();

  }

  function getByRegionCode($regionCode = null, $month = 1) {
    if ( !$regionCode )
      throw new Exception("Fail to get region.");

    $this->db->select(array(
      'Value2017 as rating_2017',
      'Value2018 as rating_2018',
      'ValueDynamic as dynamic',
      's.month',

      's.idR as param_id',
      'p.Name as param_name',
    ));
    $this->db->distinct('s.month');
    $this->db->where('r.code', $regionCode);
    $this->db->where('s.month', $month);

    $this->db->from("$this->table s");
    $this->db->join("param2018 p", "p.id = s.idP", 'inner');
    $this->db->join("region r", "r.idR = s.idR", 'right');
    return $this->db->get()->result_array();
  }
}