<?php

/**
 * Class Region_model
 *
 * @property CI_DB_query_builder $db
 */
class Region_model extends CI_Model
{

  function getList($arDisabledAreas = null)
  {

    if (!empty($arDisabledAreas))
      $this->db->where_not_in('r.idA', $arDisabledAreas);

    $this->db->from('region r');
    return $this->db->get()->result_array();

  }

  function getByAreaId($idA)
  {

    $this->db->where('idA', $idA);
    return $this->db->get('region')->result_array();

  }


}