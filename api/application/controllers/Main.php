<?php

/**
 * Class Main
 *
 */
class Main extends MY_Controller
{

  function index() {
    $this->load->view('index');
  }

  function init()
  {
    return $this->json([
      'areas' => $this->area_model->getList(),
      'params' => $this->param_model->getList(),
      'groups' => $this->group_model->getList()
    ]);
  }


}
