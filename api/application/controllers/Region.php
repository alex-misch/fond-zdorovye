<?php

/**
 * Class Region
 *
 */
class Region extends MY_Controller {


  function detail($regionCode) {

    $month = $this->input->get('month');
    return $this->json([
      'rating' => $this->stat_model->getByRegionCode($regionCode, $month)
    ]);
  }

  function getSortedList()
  {
    $arParams = (array)json_decode($this->input->get('param_list'));
    $arAreas = (array)json_decode($this->input->get('areas'));
    $arDisabledAreas = array();
    if (!empty($arAreas)) {
      foreach ($arAreas as $id => $bool)
        if (!$bool) $arDisabledAreas[] = $id;
    }

    $arRegions = $this->region_model->getList($arDisabledAreas);
    if (empty($arRegions)) return $this->json(null);

    $arRegionsByYear = $arRegionsDynamic = $ratingByYear = array();
    foreach ($arRegions as &$arRegion) {
      $arStatsByMonth = $this->stat_model->getList($arRegion['idR'], $arParams);

      foreach ($arStatsByMonth as $arStats) {
        foreach ( self::YEAR_LIST as $year ) {
          $arRegion['rating'] = $arStats["rating_$year"];
          $arRegionsByYear[ $year ][ $arStats['month'] ][$arRegion['code']] = $arRegion;
        }

        $arRegion['rating'] = $arStats['dynamic'];
        $arRegionsDynamic[$arStats['month']][$arRegion['code']] = $arRegion;
      }

    }

    foreach ($arRegionsDynamic as $month => $arDynamicMonth) {
      $arRegionsDynamic[$month] = $this->sort($arDynamicMonth, true);
    }
    $arSortedRegions = [
      'dynamic' => $arRegionsDynamic,
      'filter' => $arAreas
    ];
    foreach (self::YEAR_LIST as $year) {
      foreach ($arRegionsByYear[$year] as $month => $arMonthRatings) {
        $arSortedRegions[$year][$month] = $this->sort( $arRegionsByYear[$year][$month] );
      }
    }
    return $this->json($arSortedRegions);
  }

  private function sort($arRegions, $isDynamic = false)
  {

    $arRegionsToSort = $arRating = $arRatingsNoData = array();
    foreach ($arRegions as $key => $arRegion) {
      if (!$isDynamic && $arRegion['rating'] <= -9) {
        $arRegion['color'] = '#c6d9e5';
        $arRegion['group'] = false;
        $arRatingsNoData[trim($arRegion['code'])] = $arRegion;
        continue;
      }

      $arRegionsToSort[] = $arRegion;
      $arRating[$key] = floatval($arRegion['rating']);
    }
    array_multisort($arRating, SORT_ASC, $arRegionsToSort);


    $arRatings = $this->bindColors($arRegionsToSort);
    return array_merge($arRatings, $arRatingsNoData);
  }

  private function bindColors($arRegionsToSort)
  {
    if (count($arRegionsToSort) == 0)
      return array();
    $arRatings = array();

    foreach ($arRegionsToSort as $key => $arRegion) {

      $regionRate = intval((count($arRegionsToSort) - ($key + 1)) / count($arRegionsToSort) * 100);
      $arRegion['color'] = $this->color_model->getByIndex($regionRate);
      if ($regionRate < 33) {
        // lower than 33 percent
        $arRegion['group'] = 'lower';
      } else if ($regionRate >= 33 && $regionRate <= 66) {
        // 33-66 percent
        $arRegion['group'] = 'middle';
      } else {
        // > 66 percent
        $arRegion['group'] = 'top';
      }
      $arRegion['group_lang'] = $this->lang->line($arRegion['group']);
      $arRatings['all'][trim($arRegion['code'])] = $arRegion;
    }


    return $arRatings['all'];
  }
}