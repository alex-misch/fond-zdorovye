<?php

/**
 * Class MY_Controller
 *
 * @property CI_Lang $lang
 * @property CI_Input $input
 * @property CI_Output $output
 * @property CI_Loader $load
 *
 * @property Stat_model $stat_model
 * @property Area_model $area_model
 * @property Param_model $param_model
 * @property Group_model $group_model
 * @property Region_model $region_model
 * @property Color_model $color_model
 */
class MY_Controller extends CI_Controller {

  const YEAR_LIST = [
    '2017',
    '2018',
  ];

  public function __construct()
  {
    parent::__construct();
    $this->load->model([
      'area_model',
      'group_model',
      'param_model',
      'stat_model',
      'region_model',
      'color_model'
    ]);
    $this->lang->load('front-end/region');
  }

  protected function json($arData)
  {
    return $this->output
      ->set_content_type('application/json')
      ->set_header('Access-Control-Allow-Origin: *')
      ->set_status_header(200)
      ->set_output(json_encode($arData));
  }

}