(function () {
  'use strict';

  angular
    .module('fondZdorovye')
    .run(runBlock);

  /** @ngInject */
  function runBlock(ApiFactory, $rootScope, $timeout) {
    var vm = $rootScope;

    vm.tableIsShowed = false;
    vm.infoIsShowed = false;
    vm.isDataLoaded = false;
    vm.okrugsById = {};
    vm.selectedYear = '2017';
    vm.okrugs = {
      selected: {},
      allSelected: true
    };

    var months = [
      'Янв', 'Фев', 'Март','Апр','Май','Июн','Июл','Авг','Сент','Окт','Ноя','Дек'
    ];
    vm.month = {
      value: 1,
      options: {
        floor: 1,
        ceil: 12,
        step: 1,
        showTicksValues: true,
        translate: function(value) {
          return months[value-1];
        },
        onChange: function(id, val) {
          $rootScope.$broadcast('monthChanged', val);
        }
      }
    };

    vm.loadRegions = function () {
      ApiFactory.appInit().then(function (response) {

        vm.paramsList = response.data.params;
        vm.paramsList.forEach(function(param) {
          param.selected = true;
        });
        vm.okrugList = response.data.areas;
        vm.groupList = response.data.groups;
        for (var i = 0; i < vm.okrugList.length; i++) {
          var okrug = vm.okrugList[i];
          vm.okrugsById[okrug.idA] = okrug.name;
          vm.okrugList[i].name = okrug.name.replace('федеральный округ', '');
        }

        vm.sortRegions(null);

      });
    };
    vm.loadRegions();

    vm.defaultRegions = false;
    vm.sortRegions = function (areas, callback) {
      vm.preloader.run();

      var weights = {};
      for (var keyP in vm.paramsList) {
        var param = vm.paramsList[keyP];
        weights[param.idP] = param.weight;
      }
      var params = vm.paramsList.filter(function (param) {
        return param.selected == true;
      }).map(function(param) {
        return parseInt(param.Id, 10)
      });

      var $httpRequest = ApiFactory.getSortedRegions(areas, weights, params).then(function (response) {
        vm.preloader.complete();
        if (response.data == null) {
          vm.regions = {
            '2017': null,
            '2018': null,
            'dynamic': null
          };
          (callback || angular.noop)(false);
          return false;
        }
        var monthAvailable = Object.keys(response.data[vm.selectedYear]);
        if ( monthAvailable.indexOf( vm.month.value ) === -1 )
            vm.month.value = monthAvailable[0];

        if (!vm.defaultRegions) {
          vm.defaultRegions = response.data[vm.selectedYear][vm.month.value];
        }
        vm.regions = response.data;
        vm.isDataLoaded = true;
        (callback || angular.noop)(response.data);

      }, function () {
        return vm.preloader.complete();
      });
      if (!$httpRequest)
        return vm.preloader.complete();

    };

    vm.preloader = {
      element: angular.element('#preloader'),
      timeout: null,
      run: function () {
        if (vm.preloader.timeout)
          $timeout.cancel(vm.preloader.timeout);
        vm.preloader.element.show().width(5 + '%');
        var percentage = 0;

        $timeout(changePreloaderPercentage, 10000);

        function changePreloaderPercentage() {
          var width = ( 100 * vm.preloader.element.width() / vm.preloader.element.parent().width() );

          $timeout.cancel(vm.preloader.timeout);
          if (width > 75) {
            if (vm.preloader.timeout)
              $timeout.cancel(vm.preloader.timeout);
            return;
          }
          percentage += parseInt(Math.random() * 20, 10);
          vm.preloader.element.width(percentage + '%');
          vm.preloader.timeout = $timeout(changePreloaderPercentage, parseInt(Math.random() * 10000, 10))
        }
      },
      complete: function () {
        vm.preloader.element.width('100%');
        $timeout(function () {
          vm.preloader.element.fadeOut(
            'fast', function () {
              vm.preloader.element.width(0);
            });
        }, 500);
        return true;
      }
    };

  }

})();
