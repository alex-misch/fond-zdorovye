(function () {
  'use strict';

  angular
    .module('fondZdorovye')
    .factory('ExportFactory', ExportFactory);

  function ExportFactory ($window) {

    function xlsx(data) {

      /* generate a worksheet */
      var ws = $window.XLSX.utils.json_to_sheet(data);

      /* add to workbook */
      var wb = $window.XLSX.utils.book_new();
      $window.XLSX.utils.book_append_sheet(wb, ws, "Показатели");

      /* write workbook and force a download */
      $window.XLSX.writeFile(wb, "ratings.xlsx");
    }

    return {
      xlsx: xlsx
    };
  }
})();
