(function () {
  'use strict';

  angular
    .module('fondZdorovye')
    .directive('closeAllPopupsDirective', closeAllPopupsDirective);

  /* @ngInject */
  function closeAllPopupsDirective() {
    return {
      restrict: "EA",
      link: function (scope, element) {
        angular.element(element).on('click', function(event) {
          var li = angular.element(event.target).closest('li');
          if (!li.length)
            angular.element('li').removeClass('opened');
        });
      }
    };
  }
})();
