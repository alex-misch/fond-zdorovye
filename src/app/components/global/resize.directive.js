(function () {
  'use strict';

  angular
    .module('fondZdorovye')
    .directive('resize', resize);

  /* @ngInject */
  function resize($window) {
    return {
      restrict: "A",
      link: function (scope, element) {
        var defaultWidth = 1800, defaultHeight = 1000, defaultFont = 16, aspect = defaultWidth / defaultHeight;

        function resize() {
          var body = angular.element(element);
          element.css('font-size', body.width() / defaultWidth * defaultFont + 'px');

          if (body.width() / body.height() > aspect) {
            element.css('font-size', body.height() / defaultHeight * defaultFont + 'px');
          }
        }

        angular.element($window).on('resize', resize);
        resize();

      }
    };
  }
})();
