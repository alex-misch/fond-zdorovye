(function () {
  'use strict';

  angular
    .module('fondZdorovye')
    .factory('ApiFactory', ApiFactory);

  function ApiFactory ($location, $http) {
    var apiUrl = $location.host() === "localhost" || $location.host().indexOf('192.168') !== -1 ? "http://fondzdorovie.ru/map2018/api/index.php/" : "/";

    function init() {
      return $http.get(apiUrl + 'init/' );
    }
    function getSortedRegions(areas, weight, param) {
      return $http.get(apiUrl + 'get_sorted_regions/', {
          params: {
            areas: angular.toJson(areas),
            weight: angular.toJson(weight),
            param_list: angular.toJson(param)
          }
      });
    }

    function getRegionDetail(code, month) {
      return $http.get(apiUrl + 'region/detail/' + code + '/?month='+month );
    }

    return {
      appInit: init,
      getSortedRegions: getSortedRegions,
      getRegionDetail: getRegionDetail
    };
  }
})();
