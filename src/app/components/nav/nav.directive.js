(function () {
  'use strict';

  angular
    .module('fondZdorovye')
    .directive('navigation', navigation)
    .directive('navClickGroup', navClickGroup);

  /* @ngInject */
  function navigation() {
    return {
      restrict: 'E',
      replace: true,
      templateUrl: 'app/components/nav/nav.html'
      //controller: 'NavController',
      //controllerAs: 'nav'
    };
  }
  function navClickGroup() {
    return {
      restrict: "EA",
      link: function (scope, element) {

        var navItemsList = angular.element(element).find('li'),
            activeClass = 'opened';

        angular.forEach(navItemsList, function(item, index) {
          var $item = angular.element(item);
            $item.on('click', function(event) {
              if ($item.is('.empty')) return;
              if (index < 3)
                return false;

              if (!item.classList.contains(activeClass))
                navItemsList.removeClass(activeClass);

              if (!angular.element(event.target).closest('.country-list-overlay').length )
                $item.toggleClass(activeClass);
              angular.element(this).find('.ps-container').perfectScrollbar('update');
            });
        });
      }
    };
  }
})();
