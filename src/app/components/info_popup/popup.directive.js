(function () {
  'use strict';

  angular
    .module('fondZdorovye')
    .directive('infoPopup', infoPopup);

  /** @ngInject */
  function infoPopup() {
    return {
      restrict: 'E',
      replace: true,
      templateUrl: 'app/components/info_popup/popup.html'
    };
  }
})();
