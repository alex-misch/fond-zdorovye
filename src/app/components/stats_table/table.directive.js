(function () {
  'use strict';

  angular
    .module('fondZdorovye')
    .directive('statsTable', statsTable)
    .directive('contentTabs', contentTabs);

  /** @ngInject */
  function statsTable() {
    return {
      restrict: 'E',
      replace: true,
      templateUrl: 'app/components/stats_table/table.html',
      controller: 'TableController',
      controllerAs: 'table'
    };
  }
  function contentTabs() {
    return function(scope, element) {
      var switchers = angular.element(element).find('li');
      var contents = angular.element(element).find('section'),
        activeClassName = 'active';

      switchers.first().addClass(activeClassName);
      contents.hide().first().show();

      switchers.click(function() {
        if (this.classList.contains(activeClassName))
          return false;

        var item = angular.element(this);
        item.addClass(activeClassName).siblings().removeClass(activeClassName);
        contents.hide().eq(item.index()).show();
        scope.$broadcast('rzSliderForceRender');
      });
    }
  }
})();
