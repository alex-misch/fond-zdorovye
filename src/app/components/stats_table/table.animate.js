(function () {
  'use strict';

  angular
    .module('fondZdorovye')
    .animation('.stats-table-animate', statsTableAnimate);

  /** @ngInject */
  function statsTableAnimate($window) {
    return {
      leave: function(elem, doneFn) {
        $window.TweenMax.fromTo(elem.find('.popup'),.5, {y: 0,opacity: 1},{y:50, opacity: 0, onComplete: doneFn});
      },
      enter: function(elem, doneFn) {
        $window.TweenMax.fromTo(elem.find('.popup'),.5, {y: -50,opacity: 0},{y:0, opacity: 1});
        doneFn();
      }
    };
  }
})();
