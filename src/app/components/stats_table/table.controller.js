(function() {
  'use strict';

  angular
    .module('fondZdorovye')
    .controller('TableController', TableController);

  /** @ngInject */
  function TableController(ExportFactory, $scope, $rootScope) {
    var vm = this;

    vm.regions = {
      list: []
    };
    vm.groupsSelected = [];

    vm.getResult = function() {
      $rootScope.reInitMap = false;
      var $http = $rootScope.sortRegions($rootScope.okrugs.selected, vm.initRegions);
      if (!$http)
        $rootScope.reInitMap = true;
      //$rootScope.tableIsShowed = false;
    };
    vm.resetResult = function() {
      for (var keyP in $rootScope.paramsList) {
        $rootScope.paramsList[keyP].weight = 1;
      }
      for (var keyG in $rootScope.groupList) {
        $rootScope.groupList[keyG].weight = 1;
      }
      vm.getResult();
    };

    vm.export = function() {
      var exportData = vm.regions.list.filter(function(region) {
        return $rootScope.okrugs.selected[region.idA]
      }).map(function(rating) {
        return {
          "№": rating.position,
          "Регион": rating.name,
          "2017г.": parseFloat(rating.rating2017).toFixed(1),
          "2018г.": parseFloat(rating.rating2018).toFixed(1),
          "Динамика": parseFloat(rating.dynamic).toFixed(1)
        }
      });
      ExportFactory.xlsx(exportData)
    };

    vm.initRegions = function() {
      $rootScope.reInitMap = true;
      vm.regions.list = [];
      var selectedMonth = $rootScope.month.value;
      var i = 0;//Object.keys($rootScope.defaultRegions).length + 1;
      for (var label in $rootScope.defaultRegions) {
        i++;
        if (!$rootScope.regions['dynamic'][selectedMonth] || !$rootScope.regions['dynamic'][selectedMonth][label])
          continue;

        var region = $rootScope.regions['dynamic'][selectedMonth][label];

        var ratingDynamic = region.rating;
        var rating2018 = $rootScope.regions['2018'][selectedMonth][label].rating;
        var rating2017 = $rootScope.regions['2017'][selectedMonth][label].rating;
        var curRegion = {
          'rating2017': rating2017,
          'rating2018': rating2018,
          'dynamic': ratingDynamic,
          'position': i,
          'name': region.name,
          'code': region.code,
          'idA': region.idA
        };
        vm.regions.list.push(curRegion);
      }
    };
    // vm.refreshSlider = function () {
    //   $timeout(function () {
    //     $scope.$broadcast('rzSliderForceRender');
    //   });
    // };
    //
    $scope.$watch("tableIsShowed",function(newVal) {
      if (newVal == true) {
        // vm.refreshSlider();
        vm.okrugs.toggleAll();
        vm.initRegions();

        for (var key in $rootScope.groupList) {
          var group = $rootScope.groupList[key];
          vm.groupsSelected[group.idG] = true;
        }
      }
    });

    vm.okrugs = {
      selected: {},
      allSelected: true
    };
    vm.okrugs.toggleAll = function() {
      vm.okrugs.selected = {};
      for (var i = 0;i< $rootScope.okrugList.length;i++) {
        vm.okrugs.selected[$rootScope.okrugList[i].idA] = vm.okrugs.allSelected;
      }
    };

  }
})();
