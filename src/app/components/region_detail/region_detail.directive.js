(function () {
  'use strict';

  angular
    .module('fondZdorovye')
    .directive('regionDetail', regionDetail);

  /* @ngInject */
  function regionDetail() {
    return {
      restrict: 'E',
      replace: true,
      templateUrl: 'app/components/region_detail/region_detail.html',
      controller: 'RegionDetailController',
      controllerAs: 'region'
    };
  }

})();
