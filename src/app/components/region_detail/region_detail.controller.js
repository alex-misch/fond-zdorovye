(function() {
  'use strict';

  angular
    .module('fondZdorovye')
    .controller('RegionDetailController', RegionDetailController);

  /** @ngInject */
  function RegionDetailController(ApiFactory, $scope, $rootScope) {
    var vm = this;
    vm.cache = {};

    vm.showed = false;

    function calcTotal(stats) {
      vm.total = {
        2017: stats.reduce(function(total, stat) {
          return parseFloat(total) + parseFloat(stat.rating_2017)
        }, 0),
        2018: stats.reduce(function(total, stat) {
          return parseFloat(total) + parseFloat(stat.rating_2018)
        }, 0),
        dynamic: stats.reduce(function(total, stat) {
          return parseFloat(total) + parseFloat(stat.dynamic)
        }, 0)
      }
    }

    vm.show = function(regionCode) {
      vm.rating = {};
      if ( regionCode == null ) return false;

      ApiFactory.getRegionDetail(regionCode, $rootScope.month.value).then(function(response) {
        vm.selected = $rootScope.defaultRegions[regionCode];
        vm.rating = response.data.rating;
        calcTotal(response.data.rating);
        vm.showed = true;
      });
    };
    vm.clearCache = function() {
      vm.cache = {};
    }

  }
})();
