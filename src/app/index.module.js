(function() {
  'use strict';

  angular
    .module('fondZdorovye', [
      'ui.router',
      'perfect_scrollbar',
      'rzModule',
      'ngAnimate'
    ]);

})();
