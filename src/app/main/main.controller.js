(function () {
  'use strict';

  angular
    .module('fondZdorovye')
    .controller('MainController', MainController);

  /** @ngInject */
  function MainController($document, $scope, $rootScope, $filter) {
    var vm = this;
    var mapObject = angular.element('#svg-map-root');

    vm.changeParam = function() {
      $rootScope.sortRegions($rootScope.okrugs.selected, vm.updateColors.bind(vm));

    };
    $scope.$on('monthChanged', function () {
      vm.updateColors();
    });
    vm.updateColors = function () {
      $rootScope.region.clearCache();
      var fregion = {};
      for (var key in $rootScope.regions[$rootScope.selectedYear][$rootScope.month.value]) {
        var curRegion = $rootScope.regions[$rootScope.selectedYear][$rootScope.month.value][key];
        if (curRegion.code && $rootScope.okrugs.selected[curRegion.idA])
          fregion[curRegion.code.trim()] = curRegion.color;
      }
      mapObject.vectorMap('set', 'colors', fregion);
      setMapColors(true);
    };

    $scope.$watch('isDataLoaded', function (newVal) {
      if (newVal != true) return false;
      vm.okrugs.toggleAll();
      initMap();

    });

    vm.okrugs = {
      toggleAll: function () {
        $rootScope.okrugs.selected = {};
        for (var i = 0; i < $rootScope.okrugList.length; i++) {
          $rootScope.okrugs.selected[$rootScope.okrugList[i].idA] = $rootScope.okrugs.allSelected;
        }
      },
      changeActive: function (event, okrugId) {
        if (okrugId != 'all') {
          $rootScope.okrugs.allSelected = false;
        } else {
          vm.okrugs.toggleAll();
        }


        $rootScope.sortRegions($rootScope.okrugs.selected, setMapColors);

      }
    };
    $scope.$watch('reInitMap', function (newVal) {
      if (newVal != true) return false;
      setMapColors(true);

    });

    function setMapColors(data) {

      var fregion = {};
      for (var key in $rootScope.defaultRegions) {

        var curRegions = $rootScope.regions[$rootScope.selectedYear][$rootScope.month.value];
        if (!curRegions || !curRegions[key] || !curRegions[key].code || !data)
          fregion[$rootScope.defaultRegions[key].code.trim()] = '#ccc';
        else {
          fregion[curRegions[key].code.trim()] = curRegions[key].color;
        }

      }
      if (fregion) {
        mapObject.vectorMap('set', 'colors', fregion);
      }
    }

    function initMap() {
      var colorRegion = '#ccc', // Цвет всех регионов
        data_obj = {};

      for (var key in $rootScope.regions[$rootScope.selectedYear][$rootScope.month.value]) {
        var region = $rootScope.regions[$rootScope.selectedYear][$rootScope.month.value][key];

        if (region.code)
          data_obj[region.code.trim()] = $rootScope.regions[$rootScope.selectedYear][$rootScope.month.value][key].color;
      }


      $document.ready(function () {
        mapObject.vectorMap({
          map: 'russia_en',
          backgroundColor: 'transparent',
          borderColor: '#000',
          borderWidth: 1,
          color: colorRegion,
          colors: data_obj,
          hoverOpacity: .7,
          enableZoom: false,
          showTooltip: true,
          defaultColor: '#ccc',
          selectedColor: false,
          onRegionClick: function (element, code) {
            event.preventDefault();
            // $rootScope.selectRegion(code);
            $rootScope.region.show(code);
          },
          onLabelShow: function (event, label, code) {
            $rootScope.table.initRegions();
            $rootScope.$apply();

            var curRegion = $rootScope.table.regions.list.filter(function(region) {
              return code === region.code;
            });
            if (!curRegion || !curRegion.length) return false;

            label.html(generateTemplateLabel(curRegion[0]));
          }
        });

      });

      function generateTemplateLabel(region) {
        var html = [
          '<strong>' + region.name + '</strong><br>',
          '<ul>',
          '<li>Округ: <b>' + ($rootScope.okrugsById[region.idA]) + '</b></li>',
          '<li>Рейтинг в 2017: <b>' + $filter('number')(region.rating2017, 1) + '</b></li>',
          '<li>Рейтинг в 2018: <b>' + $filter('number')(region.rating2018, 1) + '</b></li>',
          '<li>Динамика: <b>' + $filter('number')(region.dynamic, 1) + '</b></li>',
          '<li>Локальная позиция: <b>' + (region.position) + '</b></li>',
          '<p>Нажмите на регион, чтобы узнать подробнее..</p>',
          '</ul>'
        ].join('');
        return html;
      }


    }
  }
})();
